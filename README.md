# Chat Stats API

Search for the chat statistics based on Website and Date Range.


## 1- TechStack

```
- Typescript     : 4.1.5
- NestJS         : 7.6.13
- RxJS           : 6.6.6
- Axios          : 0.21.1
- Swagger        : 4.7.16
- Jest           : 26.6.3
- Supertest      : 6.1.3
```
  

## 2- Install & Configure

```
# Compile Project Files
    yarn
    yarn build
    
# Execute Project In Production Mode    
    yarn run:prod
```


## 3- Documentation

Once the app is launched, you can open the swagger page for try the API:

Swagger API Doc Link: http://localhost:3000/docs
