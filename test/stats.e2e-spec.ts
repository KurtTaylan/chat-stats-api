import { Test, TestingModule } from '@nestjs/testing';
import { StatsController } from '../src/modules/stats/stats.controller';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { AppModule } from '../src/app.module';
import * as request from 'supertest';
import { StatsModule } from '../src/modules/stats/stats.module';

describe('StatsController', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule, StatsModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ transform: true }));

    await app.init();
  });

  it('should get stats without dates', () => {
    return request(app.getHttpServer())
      .get('/stats')
      .expect(200)
      .expect((res) => {
        expect(res.body).not.toBeNull();
        expect(res.body['count']).toEqual(8);
        expect(res.body['result']).not.toBeNull();
        expect(res.body['result'][0].chats).toEqual(1568);
        expect(res.body['result'][0].missedChats).toEqual(11);
      });
  });

  it('should get stats with dates', () => {
    return request(app.getHttpServer())
      .get('/stats')
      .query({
        startDate: '2019-04-01',
        endDate: '2019-04-06',
      })
      .expect(200)
      .expect((res) => {
        expect(res.body).not.toBeNull();
        expect(res.body['count']).toEqual(8);
        expect(res.body['result']).not.toBeNull();
        expect(res.body['result'][0].chats).toEqual(728);
        expect(res.body['result'][0].missedChats).toEqual(4);
      });
  });
});
