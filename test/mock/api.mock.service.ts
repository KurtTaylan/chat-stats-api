import { ClientStatDto } from '../../src/modules/stats/dto/client/client-stat.dto';
import { AxiosResponse } from 'axios';
import { successClient } from '../stab/success.json';

export class ApiServiceMock {
  async sinkChatStatsData(): Promise<AxiosResponse<ClientStatDto[]>> {
    return Promise.resolve({
      status: 200,
      headers: {},
      config: {},
      statusText: 'success',
      data: successClient,
    });
  }
}
