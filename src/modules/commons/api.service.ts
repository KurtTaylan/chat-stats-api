import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ClientStatDto } from '../stats/dto/client/client-stat.dto';
import { AxiosResponse } from 'axios';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

@Injectable()
export class ApiService {
  constructor(
    private httpService: HttpService,
    private configService: ConfigService,
  ) {}

  sinkChatStatsData(): Promise<AxiosResponse<ClientStatDto[]>> {
    return this.httpService
      .get<ClientStatDto[]>(this.configService.get<string>('API_URL'))
      .toPromise();
  }

  async sinkChatStatsDatan(): Promise<ClientStatDto[]> {
    return fetch(this.configService.get<string>('API_URL'))
      .then((response) => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .catch((error: Error) => {
        console.error('Error occurred during external API call: ', error);
      });
  }
}
