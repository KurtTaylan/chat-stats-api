import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';

export class RequestSearchStatDto {
  @ApiProperty({
    required: false,
    type: 'string',
    description: 'Search start date',
    example: '2019-04-01',
  })
  @Type(() => Date, {})
  startDate: Date;

  @ApiProperty({
    required: false,
    type: 'string',
    description: 'Search end date',
    example: '2019-04-14',
  })
  @Type(() => Date)
  endDate: Date;
}
