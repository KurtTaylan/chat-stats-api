export class CalculatedStatDto {
  websiteId: string;
  chats: number;
  missedChats: number;
}
