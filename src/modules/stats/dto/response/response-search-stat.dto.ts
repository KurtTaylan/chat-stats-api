import { ApiProperty } from '@nestjs/swagger';
import { CalculatedStatDto } from '../domain/calculated-stat.dto';

export class ResponseSearchStatDto {
  @ApiProperty({
    type: 'number',
    description: 'Number of Search Result Items',
    example: 1,
  })
  count: number;

  @ApiProperty({
    description: 'List of Search Result',
    type: 'array',
    items: {
      type: 'object',
      properties: {
        websiteId: {
          type: 'string',
          description: 'Individual Website id where chat stats are grouped by.',
          example: '4f8b36d00000000000000007',
        },
        chats: {
          type: 'number',
          description: 'Total Number of Chats issued by filtered dates',
          example: 1006,
        },
        missedChats: {
          type: 'number',
          description: 'Total Number of Missed Chats issued by filtered dates',
          example: 546,
        },
      },
    },
  })
  result: CalculatedStatDto[];
}
