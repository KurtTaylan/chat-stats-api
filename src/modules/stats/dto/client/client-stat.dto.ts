export class ClientStatDto {
  websiteId: string;
  date: string;
  chats: number;
  missedChats: number;
}
