import { Test, TestingModule } from '@nestjs/testing';
import { StatsService } from './stats.service';
import { RequestSearchStatDto } from './dto/request/request-search-stat.dto';
import { ApiService } from '../commons/api.service';
import { successResponseWeek } from '../../../test/stab/success.json';
import { ApiServiceMock } from '../../../test/mock/api.mock.service';

describe('StatsService', () => {
  let service: StatsService;

  beforeEach(async () => {
    const ApiServiceProvider = {
      provide: ApiService,
      useClass: ApiServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [StatsService, ApiServiceProvider],
    }).compile();

    service = module.get<StatsService>(StatsService);
  });

  it('should filter 8 websites with 7 days history', async () => {
    // given
    const requestDto = new RequestSearchStatDto();
    requestDto.startDate = new Date('2019-04-01');
    requestDto.endDate = new Date('2019-04-07');

    // when
    const response = await service.search(requestDto);

    // then
    expect(response).not.toBeNull();
    expect(response.count).toEqual(8);
    expect(response.result).toStrictEqual(successResponseWeek);
  });
});
