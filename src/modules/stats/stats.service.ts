import { Injectable, Logger } from '@nestjs/common';
import { RequestSearchStatDto } from './dto/request/request-search-stat.dto';
import { ResponseSearchStatDto } from './dto/response/response-search-stat.dto';
import { ClientStatDto } from './dto/client/client-stat.dto';
import { ApiService } from '../commons/api.service';
import { CalculatedStatDto } from './dto/domain/calculated-stat.dto';

interface GroupMap<V> {
  [websiteId: string]: V;
}

@Injectable()
export class StatsService {
  private readonly logger = new Logger(StatsService.name);

  constructor(private apiService: ApiService) {}

  async search(
    searchStatDto: RequestSearchStatDto,
  ): Promise<ResponseSearchStatDto> {
    this.logger.log(`Request Search Parameters are:
    startDate: ${searchStatDto.startDate} 
    endDate: ${searchStatDto.endDate}
    `);

    const sink: ClientStatDto[] = await this.apiService.sinkChatStatsDatan();
    const resultList: CalculatedStatDto[] = this.filter(searchStatDto, sink);

    this.logger.log(
      'Request has ended for Website Count:' + resultList?.length,
    );

    return {
      count: resultList?.length,
      result: resultList,
    };
  }

  private filter(searchStatDto: RequestSearchStatDto, list: ClientStatDto[]) {
    const resultList: CalculatedStatDto[] = [];
    const shouldNotFilterDate =
      !searchStatDto.startDate || !searchStatDto.endDate;

    const groupedByWebSiteId: GroupMap<CalculatedStatDto> = {};
    list.forEach((value) => {
      const date = new Date(value.date);
      const insideFilter: boolean =
        date >= searchStatDto.startDate && date <= searchStatDto.endDate;

      if (shouldNotFilterDate || insideFilter) {
        if (groupedByWebSiteId[value.websiteId]) {
          const existing = groupedByWebSiteId[value.websiteId];
          existing.chats = existing.chats + value.chats;
          existing.missedChats = existing.missedChats + value.missedChats;
        } else {
          groupedByWebSiteId[value.websiteId] = {
            websiteId: value.websiteId,
            chats: value.chats,
            missedChats: value.missedChats,
          };
        }
      }
    });

    for (const [key, value] of Object.entries(groupedByWebSiteId)) {
      resultList.push(value);
    }

    return resultList;
  }
}
