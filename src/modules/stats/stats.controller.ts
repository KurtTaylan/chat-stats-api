import { Controller, Get, Query } from '@nestjs/common';
import { StatsService } from './stats.service';
import { RequestSearchStatDto } from './dto/request/request-search-stat.dto';
import { ResponseSearchStatDto } from './dto/response/response-search-stat.dto';
import { ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';

@ApiTags('stats')
@Controller('stats')
export class StatsController {
  constructor(private readonly statsService: StatsService) {}

  @Get()
  @ApiOkResponse({
    type: ResponseSearchStatDto,
  })
  async search(
    @Query() searchStatDto: RequestSearchStatDto,
  ): Promise<ResponseSearchStatDto> {
    return this.statsService.search(searchStatDto);
  }
}
