import { Module } from '@nestjs/common';
import { StatsService } from './stats.service';
import { StatsController } from './stats.controller';
import { CommonsModule } from '../commons/commons.module';

@Module({
  imports: [CommonsModule],
  controllers: [StatsController],
  providers: [StatsService],
})
export class StatsModule {}
