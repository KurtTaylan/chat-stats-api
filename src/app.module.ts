import { Module } from '@nestjs/common';
import { StatsModule } from './modules/stats/stats.module';
import { ConfigModule } from '@nestjs/config';
import { CommonsModule } from './modules/commons/commons.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: './src/configs/env/.env',
    }),
    CommonsModule,
    StatsModule,
  ],
})
export class AppModule {}
